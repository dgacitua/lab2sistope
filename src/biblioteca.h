#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

int char2num (char letra) {
	return letra-65;
}

char num2char (int numero) {
        if (numero>=0 && numero<=4)     return numero+65;
        else if (numero>=4 && numero<=7) return numero+66;
        else if (numero>=8 && numero<=15) return numero+67;
        else return 45;
}

int getLanId (int id) {
	if (id>=0 && id<=3) {
		return 1;
	}
	else if (id>=4 && id<=7) {
		return 2;
	}
	else if (id>=8 && id<=11) {
		return 3;
	}
	else if (id>=12 && id<=15) {
		return 4;
	}
	else {
		return 0;
	}
}

int getPosId (int id) {
	if (id==0 || id==8 || id==10 || id==17 || id==4) {
		return 0;
	}
	else if (id==1 || id==7 || id==11 || id==16 || id==9) {
		return 1;
	}
	else if (id==2 || id==6 || id==12 || id==15) {
		return 2;
	}
	else if (id==3 || id==5 || id==13 || id==14) {
		return 3;
	}
	else {
		return -1;
	}
}

Mensaje limpiarMensaje () {
	Mensaje m;
	m.id = -1;
	m.targetId = -1;
	m.lansPassed[0] = -1;
	m.lansPassed[1] = -1;
	m.bridgesPassed[0] = -1;
	m.bridgesPassed[1] = -1;

	return m;
}

int pasaPuente(int Lan1, int Lan2){
	int i;
	if(Lan1 ==0 || Lan1 ==1 ||Lan1 ==2 || Lan1 ==3){
		if(Lan2 ==0 || Lan2 ==1 ||Lan2 ==2 || Lan2 ==3){
			i=0;
		}else{
			i=1;
		}
	}

	return i;
}

Mensaje crearMensaje (int num, int target) {
	Mensaje m;
	m.id = num;
	m.targetId = target;
	m.lansPassed[0] = FALSE;
	m.lansPassed[1] = FALSE;
	m.bridgesPassed[0] = FALSE;
	m.bridgesPassed[1] = FALSE;

	return m;
}

int todosListos (int arr[], int cant) {
	int i;
	for (i=0; i<cant; i++) {
		if (!arr[i]) {
			return FALSE;
		}
	}
	return TRUE;
}

int mensajeVacio (Mensaje m) {
	if (m.targetId == -1) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

int getBridgeId (int id) {
	if (id>=0 && id<=7) {
		return 1;
	}
	else if (id>=8 && id<=15) {
		return 2;
	}
	else {
		return 0;
	}
}

int targetedBus (int bridgeId, int targetId) {
	if (bridgeId==0) {
		if (getLanId(targetId)== 1) return 1;
		if (getLanId(targetId)== 2) return 2;
		if (getLanId(targetId)== 3) return 0;
		if (getLanId(targetId)== 4) return 0;
	}
	if (bridgeId==1) {
		if (getLanId(targetId)== 1) return 0;
		if (getLanId(targetId)== 2) return 0;
		if (getLanId(targetId)== 3) return 3;
		if (getLanId(targetId)== 4) return 4;
	}
	return -1;	
}