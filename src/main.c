#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include "estructuras.h"
#include "biblioteca.h"

#define TRUE 1
#define FALSE 0

#define NUM_ESTACIONES 8 //16
#define NUM_BRIDGES 1 //2
#define NUM_BUSES 3 //5

int NUM_MSG;
#define TIME_OUT 1
#define LARGO_MENSAJE 256

Mensaje *mensajeEnBus;
int mensajesTotales;
int estacionLista[NUM_ESTACIONES], bridging[NUM_BRIDGES];
pthread_mutex_t *buses, *bridgeLocks, *checkLocks, auxLock;

void *accionEstacion(void *arg);
void *accionBridge(void *arg);
void printBuses();
void printMems(Bridge *b);
void printEstadoSt();

int main (int argc, char **argv) {
	int i, j;

	//getopt variables
	int c;
	char *flag;

	////////////Getopt///////////////
	while (1)
    {
      static struct option long_options[] =
        {
          
          {"send",   required_argument, 0, 's'},
          {0, 0, 0, 0}
        };
      
      int option_index = 0;

      c = getopt_long (argc, argv, "s:",long_options, &option_index);
		  if (c == -1)
        break;

      switch (c)
        {
        case 's':
          flag=optarg;
          break;

        case '?':
          return 1;
          break;

        default:
          abort ();
          return 1;
        }
    }
	
    NUM_MSG=atoi(flag);
	////////////End-Getopt//////////
	///////////Open File////////////////
    FILE *salida=fopen("src/salida","w");
    fclose(salida);
    //fprintf(salida,"holi\n" );
    //fclose(salida);
    //////////////////////////////


	pthread_t *estaciones, *bridges;

	Estacion *paramSt;
	Bridge *bridgeSt;

	estaciones = (pthread_t*)malloc(NUM_ESTACIONES*sizeof(pthread_t));
	bridges = (pthread_t*)malloc(NUM_BRIDGES*sizeof(pthread_t));

	buses = (pthread_mutex_t*)malloc(NUM_BUSES*sizeof(pthread_mutex_t));
	bridgeLocks = (pthread_mutex_t*)malloc(NUM_BRIDGES*sizeof(pthread_mutex_t));
	checkLocks = (pthread_mutex_t*)malloc(NUM_BRIDGES*sizeof(pthread_mutex_t));

	mensajeEnBus = (Mensaje*)malloc(NUM_BUSES*sizeof(Mensaje));

	for (i=0; i<NUM_BUSES; i++) {
		mensajeEnBus[i] = limpiarMensaje();
		pthread_mutex_init(&buses[i],NULL);
	}

	for (i=0; i<NUM_BRIDGES; i++) {
		bridging[i] = FALSE;
		pthread_mutex_init(&checkLocks[i],NULL);
		pthread_mutex_init(&bridgeLocks[i],NULL);
	}
	
	pthread_mutex_init(&auxLock,NULL);

	paramSt = (Estacion*)malloc(NUM_ESTACIONES*sizeof(Estacion));
	bridgeSt = (Bridge*)malloc(NUM_BRIDGES*sizeof(Bridge));

	

	for (i=0; i<NUM_ESTACIONES; i++) {
		paramSt[i].id = i;
		paramSt[i].lanId = getLanId(paramSt[i].id);
		paramSt[i].flag1 = paramSt[i].flag2 = TRUE;
		paramSt[i].cont1 = paramSt[i].cont2 = 0;
		paramSt[i].mensaje = (Mensaje*)malloc((NUM_MSG*NUM_ESTACIONES)*sizeof(Mensaje));

		for (j=0; j<(NUM_MSG*NUM_ESTACIONES); j++) {
			paramSt[i].mensaje[j] = crearMensaje(j,(paramSt[i].id+j+1)%NUM_ESTACIONES);
		}

		estacionLista[i] = FALSE;
	}

	for (i=0; i<NUM_BRIDGES; i++) {
		bridgeSt[i].id = i;
		for (j=0; j<4; j++) {
			bridgeSt[i].mem[j] = limpiarMensaje();
		}
	}

	for (i=0; i<NUM_ESTACIONES; i++) {
		pthread_create(&(estaciones[i]), NULL, &accionEstacion, (void*)&(paramSt[i]));
	}

	for (i=0; i<NUM_BRIDGES; i++) {
		pthread_create(&(bridges[i]), NULL, &accionBridge, (void*)&(bridgeSt[i]));
	}

	for (i=0; i<NUM_ESTACIONES; i++) {
		pthread_join(estaciones[i], NULL);
	}

	for (i=0; i<NUM_BRIDGES; i++) {
		pthread_join(bridges[i], NULL);
	}

	fclose(salida);
	return 0;
}

void *accionEstacion(void *arg) {
	Estacion *p = (Estacion*)arg;

	

	while (p->flag1 || p->flag2) {
		
		pthread_mutex_lock(&buses[p->lanId]);
		if (!mensajeVacio(mensajeEnBus[p->lanId]) && mensajeEnBus[p->lanId].targetId==p->id) {
			//pthread_mutex_lock(&auxLock);
			//FILE *salida=fopen("src/salida","a");
			//fprintf(salida,"El PC %c recibió su mensaje Nº%d\n",num2char(mensajeEnBus[p->lanId].targetId), p->cont1);
			//fclose(salida);
			mensajeEnBus[p->lanId] = limpiarMensaje();
			//pthread_mutex_unlock(&auxLock);
			p->cont1++;			
		}
		pthread_mutex_unlock(&buses[p->lanId]);

		pthread_mutex_lock(&buses[p->lanId]);
		if (mensajeVacio(mensajeEnBus[p->lanId]) && p->cont2<NUM_MSG) {
			//pthread_mutex_lock(&auxLock);
			mensajeEnBus[p->lanId] = p->mensaje[p->cont2];
			p->mensaje[p->cont2] = limpiarMensaje();
			//pthread_mutex_unlock(&auxLock);
			if(pasaPuente(mensajeEnBus[p->lanId].id,mensajeEnBus[p->lanId].targetId)==0){
				FILE *salida=fopen("src/salida","a");
				fprintf(salida,"Equipo %c manda mensaje a %c desde la LAN%d \n",num2char(mensajeEnBus[p->lanId].id),num2char(mensajeEnBus[p->lanId].targetId), p->lanId);
				//fprintf(salida,"Se envió el mensaje Nº%d al PC %c desde la LAN%d\n",p->cont2,num2char(mensajeEnBus[p->lanId].targetId), p->lanId);
				fclose(salida);
			}else{
				FILE *salida=fopen("src/salida","a");
				fprintf(salida,"Equipo %c manda mensaje a %c desde la LAN%d pasando por el puente 1\n",num2char(mensajeEnBus[p->lanId].id),num2char(mensajeEnBus[p->lanId].targetId), p->lanId);
				//fprintf(salida,"Se envió el mensaje Nº%d al PC %c desde la LAN%d\n",p->cont2,num2char(mensajeEnBus[p->lanId].targetId), p->lanId);
				fclose(salida);
			}
			p->cont2++;
		}
		pthread_mutex_unlock(&buses[p->lanId]);
		
		if (p->cont1>=NUM_MSG && p->cont2>=NUM_MSG) {
			pthread_mutex_lock(&auxLock);
			estacionLista[p->id] = TRUE;

			pthread_mutex_unlock(&auxLock);
			pthread_exit(NULL);
		}
	}
	pthread_exit(NULL);
}

void *accionBridge(void *arg) {
	Bridge *b = (Bridge*)arg;
	int flag1, flag2, targetBus;
	Mensaje aux;

	
	flag1 = flag2 = TRUE;


	while (flag1) {
		// Bridge 1
		if (b->id==0) {
			//printMems(b);
			//printBuses();
			// LAN 1
			pthread_mutex_lock(&buses[1]);
			pthread_mutex_lock(&bridgeLocks[0]);
			//printf("var1 %d\n", mensajeEnBus[1].targetId);
			if (!mensajeVacio(b->mem[1])) {
				targetBus = targetedBus(b->id,b->mem[1].targetId);
				if (mensajeVacio(mensajeEnBus[targetBus])) {
					mensajeEnBus[targetBus] = b->mem[1];
					b->mem[1] = limpiarMensaje();
					//printf("c1\n");	
				}				
			}

			if (!mensajeVacio(mensajeEnBus[1]) && getLanId(mensajeEnBus[1].targetId)!=1) {
				b->mem[1] = mensajeEnBus[1];
				mensajeEnBus[1] = limpiarMensaje();
				//printf("a1\n");
			}
			pthread_mutex_unlock(&bridgeLocks[0]);
			pthread_mutex_unlock(&buses[1]);

			// LAN 2
			pthread_mutex_lock(&buses[2]);
			pthread_mutex_lock(&bridgeLocks[0]);
			//printf("var2 %d\n", mensajeEnBus[2].targetId);
			if (!mensajeVacio(b->mem[2])) {
				targetBus = targetedBus(b->id,b->mem[2].targetId);
				if (mensajeVacio(mensajeEnBus[targetBus])) {
					mensajeEnBus[targetBus] = b->mem[2];
					b->mem[2] = limpiarMensaje();
					//printf("c2\n");	
				}
			}

			if (!mensajeVacio(mensajeEnBus[2]) && getLanId(mensajeEnBus[2].targetId)!=2) {
				b->mem[2] = mensajeEnBus[2];
				mensajeEnBus[2] = limpiarMensaje();
				//printf("a2\n");
			}
			pthread_mutex_unlock(&bridgeLocks[0]);
			pthread_mutex_unlock(&buses[2]);

			pthread_mutex_lock(&bridgeLocks[0]);
			if (!mensajeVacio(b->mem[1]) && !mensajeVacio(b->mem[2])) {
				if (b->mem[1].targetId==b->mem[2].targetId) {		// FIX a condicion de borde
					//printf("jai %d\n",b->mem[1].targetId);
					//pthread_mutex_unlock(&bridgeLocks[0]);
					//pthread_exit(NULL);
					aux = b->mem[1];
					if (targetedBus(b->id,aux.targetId)==1) {
						b->mem[2] = limpiarMensaje();
					}
					else if (targetedBus(b->id,aux.targetId)==2) {
						b->mem[1] = limpiarMensaje();
					}
					else {
						b->mem[1] = limpiarMensaje();
						b->mem[2] = limpiarMensaje();
					}
					aux = limpiarMensaje();
				}
				else {
					aux = b->mem[1];
					b->mem[1] = b->mem[2];
					b->mem[2] = aux;
					aux = limpiarMensaje();
					//printf("b1\n");	
				}				
			}
			
			if (!mensajeVacio(b->mem[1]) && mensajeVacio(b->mem[2])) {
				b->mem[2] = b->mem[1];
				b->mem[1] = limpiarMensaje();
				//printf("b2\n");
			}
			
			if (mensajeVacio(b->mem[1]) && !mensajeVacio(b->mem[2])) {
				b->mem[1] = b->mem[2];
				b->mem[2] = limpiarMensaje();
				//printf("b3\n");
			}
			pthread_mutex_unlock(&bridgeLocks[0]);

			//printMems(b);
			//printBuses();
			//printf("chai\n");
		}

		// Bridge 2
		if (b->id==1) {

		}
		//printf("chai2\n");
		//printEstadoSt();

		if (todosListos(estacionLista,NUM_ESTACIONES)) {
			flag1 = FALSE;
			pthread_exit(NULL);
		}
	}
	pthread_exit(NULL);
}





