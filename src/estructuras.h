#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>

typedef struct mensaje_struct {
	int id, targetId;
	int lansPassed[2], bridgesPassed[2];
} Mensaje;

typedef struct estacion_struct {
	int id, lanId, flag1, flag2, cont1, cont2;
	Mensaje *mensaje;
} Estacion;

typedef struct bridge_struct {
	int id;
	Mensaje mem[4];
} Bridge;